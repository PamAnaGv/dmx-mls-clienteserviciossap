/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.demexis.cliente.sap.foliador.impl;

import com.demexis.cliente.sap.foliador.FormConsultaFoliosSucRecueste;
import com.demexis.cliente.sap.foliador.FormConsultaFoliosUserRecueste;
import com.demexis.cliente.sap.foliador.TblFormlConsultaFoliosSucResponse;
import com.demexis.cliente.sap.foliador.TblFormlConsultaFoliosUserResponse;
import com.demexis.cliente.sap.foliador.WsFoliador;
import com.demexis.cliente.sap.foliador.WsFoliador_Service;
import java.net.URL;

/**
 *
 * @author Pame
 */
public class ClienteSAPFoliadorImpl {

    /**
     * Método que invoca la funcion wsConsultaFoliosUser del WS-Foliador, recibe
     * un parámetro extra que es la URL del servicio.
     *
     * @author: Demexis
     * @version: 1.0
     * @param request
     * @param url
     * @return: response del servicio invocado
     */
    public TblFormlConsultaFoliosUserResponse consultaFoliosUsuario(FormConsultaFoliosUserRecueste request, URL url) {
        WsFoliador_Service service = new WsFoliador_Service(url);
        WsFoliador port = service.getWsFoliadorPort();
        return port.wsConsultaFoliosUser(request);
    }

    /**
     * Método que invoca la funcion wsConsultaFoliosUser del WS-Foliador, recibe
     * un parámetro extra que es la URL del servicio.
     *
     * @author: Demexis
     * @version: 1.0
     * @param request
     * @param url
     * @return: response del servicio invocado
     */
    public TblFormlConsultaFoliosSucResponse consultaFoliosSucursal(FormConsultaFoliosSucRecueste request, URL url) {
        WsFoliador_Service service = new WsFoliador_Service(url);
        WsFoliador port = service.getWsFoliadorPort();
        return port.wsConsultaFoliosSuc(request);

    }

    public static void main(String[] arg) {
        try {
            ClienteSAPFoliadorImpl servicio = new ClienteSAPFoliadorImpl();
            URL url = new URL("http://10.20.0.97:8080/wsFolios/wsFoliador?WSDL");
            FormConsultaFoliosUserRecueste request = new FormConsultaFoliosUserRecueste();
            request.setIIdSuc(1200);
            //request.setSAsignacion("1188");
            request.setSCveOperacion("ASGUSR");
            request.setSCveEstatus("A");
            TblFormlConsultaFoliosUserResponse respuesta = servicio.consultaFoliosUsuario(request, url);
            System.out.println(respuesta.getRecuesteEstatus().getSDescripcionError());
            System.out.println(respuesta.getRecuesteEstatus().getIdRecuest());
            System.out.println(respuesta.getListFormConsultaFoliosUserResponse().size());

            FormConsultaFoliosSucRecueste requestSuc = new FormConsultaFoliosSucRecueste();
            requestSuc.setIIdSuc(1200);
            requestSuc.setSCveEstatus("A");
            requestSuc.setSCveOperacion("RECSUC");
            TblFormlConsultaFoliosSucResponse response = servicio.consultaFoliosSucursal(requestSuc, url);
            System.out.println(response.getRecuesteEstatus().getSDescripcionError());
            System.out.println(response.getRecuesteEstatus().getIdRecuest());
            System.out.println(response.getListFormConsultaFoliosSucResponse().size());

        } catch (Exception excp) {
            excp.printStackTrace();
        }

    }

}
